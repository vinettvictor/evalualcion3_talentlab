package com.everis.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluacionSumativa3Application {

	public static void main(String[] args) {
		SpringApplication.run(EvaluacionSumativa3Application.class, args);
	}

}
