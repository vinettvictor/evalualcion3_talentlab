package com.everis.data.services;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.data.models.Producto;
import com.everis.data.models.Venta;
import com.everis.data.repositories.VentaRepository;

@Service
public class VentaService {

	
	@Autowired
	private VentaRepository ventaRepository;
	
	public void crearVenta(@Valid Venta venta) {
		ventaRepository.save(venta);

		
	}

}
