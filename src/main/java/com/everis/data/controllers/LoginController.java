package com.everis.data.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/inicio")
public class LoginController {
	
	@RequestMapping("")
	public String index() {		
		return "inicio.jsp";
	}

}
