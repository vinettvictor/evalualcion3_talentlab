package com.everis.data.controllers;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.everis.data.models.Usuario;
import com.everis.data.services.UsuarioService;

@Controller
public class UsuarioController {
	@Autowired
	UsuarioService usuarioService;
	
	@RequestMapping("/login")
	public String login() {
		return "login.jsp";
	}
	
	@RequestMapping("/registro")
	public String registro(@ModelAttribute("usuario") Usuario usuario) {
		return "registro.jsp";
	}
	
	@RequestMapping("/ingresar")
	public String ingresar(@RequestParam("email") String email,
			@RequestParam("password") String password,
			HttpSession session
			) {
		boolean exiteUsuario = usuarioService.validarUsuario(email, password);
		if(exiteUsuario) {
			Usuario usuario = usuarioService.findByEmail(email);
			//guardando un elemento en session
			session.setAttribute("usuarioId", usuario.getId());
			return "home.jsp";
		}
		
		return "redirect:/login";
	}
	
	@RequestMapping("/registrar")
	public String registrar(@Valid @ModelAttribute("usuario") Usuario usuario) {
		//llamar a las validaciones
		usuarioService.save(usuario);
		return "redirect:/login";
	}
	
	@RequestMapping("/home")
	public String home(HttpSession session){
		//valida el acceso a rutas
		if(session.getAttribute("usuarioId")!=null) {
			return "home.jsp";
		}
		return "redirect:/login";
		
	}
	
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		
		if(session.getAttribute("usuarioId")!=null) {
			session.invalidate();//matamos todas las variables de session
		}
		return "redirect:/login";
	}

}
